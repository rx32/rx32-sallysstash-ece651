package edu.duke.ece651.sallysstash.stacks;

import edu.duke.ece651.sallysstash.input.Parser;

import java.util.HashMap;

public class CrazyStack extends Stack{

  CrazyStack(StackType stackType, Parser.PlaceStackInfo placeStackInfo) {
    super(stackType, placeStackInfo);
    name = "Blue";
    stackChar = 'B';
    numCells = 6;
    dirToRelative = new HashMap<>(){{
      put(Direction.UP, new int[][]{{0, 0}, {1, 0}, {2, 0}, {2, 1}, {3, 1}, {4, 1}});
      put(Direction.DOWN, new int[][]{{0, 0}, {1, 0}, {2, 0}, {2, -1}, {3, -1}, {4, -1}});
      put(Direction.LEFT, new int[][]{{0, 0}, {0, 1}, {0, 2}, {1, 2}, {1, 3}, {1, 4}});
      put(Direction.RIGHT, new int[][]{{0, 0}, {0, 1}, {0, 2}, {-1, 2}, {-1, 3}, {-1, 4}});
    }};
  }

}

