package edu.duke.ece651.sallysstash.stacks;

import edu.duke.ece651.sallysstash.input.Parser;

import static edu.duke.ece651.sallysstash.stacks.StackType.*;

public class StackFactory {
  public Stack getStack(StackType stackType, Parser.PlaceStackInfo placeStackInfo) {
    if (stackType == PURPLE) {
      return new Len3RectangleStack(stackType, placeStackInfo);
    } else if (stackType == GREEN) {
      return new Len6RectangleStack(stackType, placeStackInfo);
    } else if (stackType == RED) {
      return new SuperStack(stackType, placeStackInfo);
    } else if (stackType == BLUE) {
      return new CrazyStack(stackType, placeStackInfo);
    }
    throw new IllegalArgumentException("No such stack type. Please check your input!");
  }
}
