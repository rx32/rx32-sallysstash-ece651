package edu.duke.ece651.sallysstash.stacks;

import edu.duke.ece651.sallysstash.input.Parser;

import java.util.HashMap;

public class Len3RectangleStack extends Stack {

  Len3RectangleStack(StackType stackType, Parser.PlaceStackInfo placeStackInfo) {
    super(stackType, placeStackInfo);
    name = "Purple";
    stackChar = 'P';
    numCells = 3;
    dirToRelative = new HashMap<>(){{
      put(Direction.HORIZONTAL, new int[][]{{0, 0}, {0, 1}, {0, 2}});
      put(Direction.VERTICAL, new int[][]{{0, 0}, {1, 0}, {2, 0}});
    }};
  }
}
