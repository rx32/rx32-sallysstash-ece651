package edu.duke.ece651.sallysstash.stacks;

public enum Direction {
  VERTICAL,
  HORIZONTAL,
  UP,
  DOWN,
  LEFT,
  RIGHT
}
