package edu.duke.ece651.sallysstash.stacks;

public enum StackType {
  GREEN,
  PURPLE,
  RED,
  BLUE
}
