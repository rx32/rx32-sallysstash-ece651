package edu.duke.ece651.sallysstash.stacks;

import edu.duke.ece651.sallysstash.input.Parser;
import javafx.util.Pair;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class Stack {
  int numCells;
  char stackChar;
  int referencePointX;
  int referencePointY;
  Direction direction;
  StackType stackType;
  public Set<Integer> found;
  String name;
  Map<Direction, int[][]> dirToRelative;
  Map<Pair<Integer, Integer>, Integer> coordinateToSquareID;

  Stack(StackType stackType, Parser.PlaceStackInfo placeStackInfo) {
    found = new HashSet<>();
    coordinateToSquareID = new HashMap<>();
    referencePointX = placeStackInfo.x;
    referencePointY = placeStackInfo.y;
    direction = placeStackInfo.direction;
    this.stackType = stackType;
  }

  public String getName() {
    return name;
  }

  public char getStackChar() {
    return stackChar;
  }

  public int getNumCells() {
    return numCells;
  }

  public StackType getStackType() {
    return stackType;
  }

  /**
   * check if the stack object is landed on a valid location
   * @param board the board to place the stack
   * @return true if valid
   */
  public boolean checkIfPositionValid(char[][] board) {
    int[][] relativeList = dirToRelative.get(direction);
    for (int[] pos : relativeList) {
      int x = referencePointX + pos[0];
      int y = referencePointY + pos[1];
      if (x < 0 || x >= board.length || y < 0 || y >= board[0].length || board[x][y] != ' ') {
        return false;
      }
    }
    return true;
  }

  /**
   * put the stack object on the board
   * @param board the board to place the stack
   * @param coordinateToStack true if valid
   */
  public void putStackOnBoard(char[][] board, Map<Pair<Integer, Integer>, Stack> coordinateToStack) {
    int[][] relative = dirToRelative.get(direction);
    for (int i = 0; i < relative.length; i++) {
      int x = referencePointX + relative[i][0];
      int y = referencePointY + relative[i][1];
      if (found.contains(i)) {
        board[x][y] = '*';
      } else {
        board[x][y] = stackChar;
      }
      coordinateToStack.put(new Pair<>(x, y), this);
      coordinateToSquareID.put(new Pair<>(x, y), i);
    }
  }

  /**
   * remove the stack from the board
   * @param board the board to place the stack
   * @param coordinateToStack true if valid
   */
  public void removeFromBoard(char[][] board, Map<Pair<Integer, Integer>, Stack> coordinateToStack) {
    int[][] relativeList = dirToRelative.get(direction);
    for (int[] pos : relativeList) {
      int x = referencePointX + pos[0];
      int y = referencePointY + pos[1];
      board[x][y] = ' ';
      coordinateToStack.remove(new Pair<>(x, y));
      coordinateToSquareID.remove(new Pair<>(x, y));
    }
  }

  /**
   * mark the corresponding square is found
   * @param pair coordinate
   */
  public void markFound(Pair<Integer, Integer> pair) {
    found.add(coordinateToSquareID.get(pair));
  }

}
