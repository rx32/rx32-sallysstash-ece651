package edu.duke.ece651.sallysstash.stacks;

import edu.duke.ece651.sallysstash.input.Parser;

import java.util.HashMap;

public class Len6RectangleStack extends Stack {
  Len6RectangleStack(StackType stackType, Parser.PlaceStackInfo placeStackInfo) {
    super(stackType, placeStackInfo);
    name = "Green";
    stackChar = 'G';
    numCells = 2;
    dirToRelative = new HashMap<>(){{
      put(Direction.HORIZONTAL, new int[][]{{0, 0}, {0, 1}});
      put(Direction.VERTICAL, new int[][]{{0, 0}, {1, 0}});
    }};
  }
}
