package edu.duke.ece651.sallysstash.stacks;

import edu.duke.ece651.sallysstash.input.Parser;

import java.util.HashMap;

public class SuperStack extends Stack {

  SuperStack(StackType stackType, Parser.PlaceStackInfo placeStackInfo) {
    super(stackType, placeStackInfo);
    name = "Red";
    stackChar = 'R';
    numCells = 4;
    dirToRelative = new HashMap<>(){{
      put(Direction.UP, new int[][]{{0, 0}, {1, -1}, {1, 0}, {1, 1}});
      put(Direction.DOWN, new int[][]{{0, 0}, {0, 1}, {1, 1}, {0, 2}});
      put(Direction.LEFT, new int[][]{{0, 0}, {1, -1}, {1, 0}, {2, 0}});
      put(Direction.RIGHT, new int[][]{{0, 0}, {1, 0}, {1, 1}, {2, 0}});
    }};
  }



}
