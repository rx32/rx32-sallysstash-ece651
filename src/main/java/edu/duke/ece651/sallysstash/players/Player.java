package edu.duke.ece651.sallysstash.players;

import edu.duke.ece651.sallysstash.controllers.*;
import edu.duke.ece651.sallysstash.input.Parser;
import edu.duke.ece651.sallysstash.stacks.StackType;
import javafx.util.Pair;

import java.util.LinkedHashMap;
import java.util.Map;

import static edu.duke.ece651.sallysstash.stacks.StackType.*;
import static edu.duke.ece651.sallysstash.stacks.StackType.BLUE;

public class Player {

  Map<StackType, Integer> stashPlaceMap = new LinkedHashMap<>() {{
    put(GREEN, 2);
    put(PURPLE, 3);
    put(RED, 3);
    put(BLUE, 2);
  }};

  String name;
  public Board board;
  boolean win;
  int remainingNewMoves = 3;

  Player( String name) {
    board = new Board();
    win = false;
    this.name = name;
  }

  public String getName() {
    return name;
  }


  public int getRemainingNewMoves() {
    return remainingNewMoves;
  }

  public boolean isWin() {
    return win;
  }

  public void setWin(boolean win) {
    this.win = win;
  }

  public void decreaseRemainingNewMoves() {
    remainingNewMoves--;
  }

  public void placeStashSinglePlayer(PlaceStashController placeStashController) {
    throw new UnsupportedOperationException("placeStashSinglePlayer not implement");
  }


  public void placeAStack(StackType stackType, int stackIdx, PlaceStashController placeStashController) {
    throw new UnsupportedOperationException("placeAStack not implement");
  }

  public String getMoveOption(MoveChoiceController moveChoiceController, Player opponent) {
    throw new UnsupportedOperationException("getMoveOption not implement");
  }

  public Pair<Integer, Integer> digASquare(DigASquareController digASquareController, Player opponent) {
    throw new UnsupportedOperationException("digASquare not implement");
  }

  public Pair<Integer, Integer> getACoordinateOnStack(MoveStackController moveStackController) {
    throw new UnsupportedOperationException("getACoordinateOnStack not implement");
  }

  public Parser.MoveInfo getMoveInfo(MoveStackController moveStackController) {
    throw new UnsupportedOperationException("getMoveInfo not implement");
  }

  public Pair<Integer, Integer> getSonarScanCoordinate(SonarScanController sonarScanController) {
    throw new UnsupportedOperationException("getSonarScanCoordinate not implement");
  }

  public void whosTurnPrompt() {
    throw new UnsupportedOperationException("whosTurnPrompt not implement");
  }

  public void isHitPrompt(boolean isHit, Player opponent) {
    throw new UnsupportedOperationException("isHitPrompt not implement");
  }

  public void specialMovePrompt() {
    throw new UnsupportedOperationException("specialMovePrompt not implement");
  }

  public void receiveSonarScanResult(Board.SonarScanResult sonarScanResult) {
    throw new UnsupportedOperationException("specialMovePrompt not implement");
  }

}
