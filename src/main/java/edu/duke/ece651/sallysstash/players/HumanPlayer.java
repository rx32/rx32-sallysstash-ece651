package edu.duke.ece651.sallysstash.players;

import edu.duke.ece651.sallysstash.controllers.*;
import edu.duke.ece651.sallysstash.input.Parser;
import edu.duke.ece651.sallysstash.stacks.StackType;
import edu.duke.ece651.sallysstash.views.*;
import javafx.util.Pair;

import java.util.Map;


public class HumanPlayer extends Player {

  public PlaceStashView placeStashView;
  public DigASquareView digASquareView;
  public MoveStackView moveStackView;
  public MoveChoiceView moveChoiceView;
  public SonarScanView sonarScanView;
  boolean isDebug = true;

  public HumanPlayer(String name) {
    super(name);
    placeStashView = new PlaceStashView();
    digASquareView = new DigASquareView();
    moveStackView = new MoveStackView();
    moveChoiceView = new MoveChoiceView();
    sonarScanView = new SonarScanView();
  }

  /**
   * place the stash for the player
   *
   * @param placeStashController placeStashController
   */
  @Override
  public void placeStashSinglePlayer(PlaceStashController placeStashController) {
    for (Map.Entry<StackType, Integer> entry : stashPlaceMap.entrySet()) {
      for (int i = 0; i < entry.getValue(); i++) {
        placeAStack(entry.getKey(), i, placeStashController);
      }
    }
    placeStashView.finalBoardPrompt();
    placeStashView.displayBoard(this);
  }

  /**
   * place a single stack, handle users input. Only when user input a valid coordinate + direction, the stack will be placed
   *
   * @param stackType the type of the stack to place
   * @param stackIdx the index of the stack to place
   * @param placeStashController placeStashController
   */
  @Override
  public void placeAStack(StackType stackType, int stackIdx, PlaceStashController placeStashController) {
    placeStashView.display(stackType, stackIdx, this);
    while (!placeStashController.handleInput(stackType, this)) {
      placeStashView.invalidInputPrompt();
      placeStashView.display(stackType, stackIdx, this);
    }
  }

  public String getMoveOption(MoveChoiceController moveChoiceController, Player opponent) {
    moveChoiceController.moveChoiceView.displayMoveChoice(this, moveChoiceController);
    moveChoiceController.doubleBoardsView.display(this.board, opponent.board);
    return moveChoiceController.getValidOptionInput();
  }

  /**
   * get the coordinate the player want to dig
   *
   * @param digASquareController digSquareController
   * @param opponent the current player's opponent
   * @return return the coordinate player want to dig
   */
  @Override
  public Pair<Integer, Integer> digASquare(DigASquareController digASquareController, Player opponent) {
    digASquareView.display(this, opponent);
    return digASquareController.getInputCoordinate();
  }

  public Pair<Integer, Integer> getACoordinateOnStack(MoveStackController moveStackController) {
    return moveStackController.getInputCoordinate();
  }

  public Parser.MoveInfo getMoveInfo(MoveStackController moveStackController) {
    return moveStackController.getInputMoveInfo();
  }

  public Pair<Integer, Integer> getSonarScanCoordinate(SonarScanController sonarScanController) {
    return sonarScanController.getUserInputSonarScanCoordinate();
  }

  public void whosTurnPrompt() {
    moveChoiceView.whosTurnPromptHuman(this);
  }

  /**
   * prompt after user digs a square in the matrix
   *
   * @param isHit if
   * @param opponent current player's opponent
   */
  @Override
  public void isHitPrompt(boolean isHit, Player opponent) {
    digASquareView.isHitPromptHuman(isHit);
  }

  @Override
  public void specialMovePrompt() {
    moveChoiceView.specialMovePrompt(this);
  }

  @Override
  public void receiveSonarScanResult(Board.SonarScanResult sonarScanResult) {
    sonarScanView.displaySonarScanResult(sonarScanResult);
  }
}
