package edu.duke.ece651.sallysstash.players;

import edu.duke.ece651.sallysstash.input.Parser;
import edu.duke.ece651.sallysstash.stacks.Direction;
import edu.duke.ece651.sallysstash.stacks.Stack;
import edu.duke.ece651.sallysstash.stacks.StackFactory;
import edu.duke.ece651.sallysstash.stacks.StackType;
import javafx.util.Pair;

import java.util.*;


public class Board {


  public static class SonarScanResult {
    public int numGreen;
    public int numPurple;
    public int numRed;
    public int numBlue;
    SonarScanResult(Map<StackType, Integer> stackScannedCounter) {
      this.numGreen = stackScannedCounter.getOrDefault(StackType.GREEN, 0);
      this.numPurple = stackScannedCounter.getOrDefault(StackType.PURPLE, 0);
      this.numRed = stackScannedCounter.getOrDefault(StackType.RED, 0);
      this.numBlue = stackScannedCounter.getOrDefault(StackType.BLUE, 0);
    }

  }

  StackFactory stackFactory;
  List<Stack> stackList = new ArrayList<>();
  Map<Pair<Integer, Integer>, Stack> coordinateToStack = new HashMap<>();
  char[][] boardMatrix;
  char[][] boardMatrixForOpponent;
  int numCells;
  int foundCells;

  public Board() {
    boardMatrix = new char[20][10];
    boardMatrixForOpponent = new char[20][10];
    for (int i = 0; i < boardMatrix.length; i++) {
      Arrays.fill(boardMatrix[i], ' ');
      Arrays.fill(boardMatrixForOpponent[i], ' ');
    }
    stackFactory = new StackFactory();
    numCells = 0;
    foundCells = 0;
  }

  public char[][] getBoardMatrix() {
    return boardMatrix;
  }

  public char[][] getBoardMatrixForOpponent() {
    return boardMatrixForOpponent;
  }

  public Stack getHitStack(Pair<Integer, Integer> coordinate) {
    return coordinateToStack.get(coordinate);
  }

  /**
   * place the stash according to user input
   *
   * @param stackType the type of the stack
   * @param placeStackInfo the direction + coordinate of the stack
   * @return if successfully placed the stack
   */
  public boolean placeStash(StackType stackType, Parser.PlaceStackInfo placeStackInfo) {
    Stack stack = stackFactory.getStack(stackType, placeStackInfo);
    if (stack.checkIfPositionValid(boardMatrix)) {
      stack.putStackOnBoard(boardMatrix, coordinateToStack);
      stackList.add(stack);
      numCells += stack.getNumCells();
      return true;
    }
    return false;
  }

  /**
   * check if there is a stash at the coordinate
   *
   * @param coordinate coordinate
   * @return return true if found else return false
   */
  public boolean findStash(Pair<Integer, Integer> coordinate) {
    int x = coordinate.getKey();
    int y = coordinate.getValue();
    if (boardMatrix[x][y] != ' ') {
      Stack stack = coordinateToStack.get(coordinate);
      if (stack == null || boardMatrix[x][y] == '*') {
        return false;
      }
      boardMatrixForOpponent[x][y] = stack.getStackChar();
      stack.markFound(coordinate);
      boardMatrix[x][y] = '*';
      foundCells++;
      return true;
    } else {
      boardMatrixForOpponent[x][y] = 'X';
      return false;
    }

  }

  /**
   * perform sonar scan on the board
   *
   * @param coordinate the coordinate the user wants to scan
   * @return sonarScanResult (number of stacks been scanned)
   */
  public SonarScanResult sonarScan(Pair<Integer, Integer> coordinate) {
    int x = coordinate.getKey();
    int y = coordinate.getValue();

    Map<StackType, Integer> stackScannedCounter = new HashMap<>();

    for (int i = x - 3; i <= x; i++) {
      for (int j = y - (3 - (x - i)); j <= y + (3 - (x - i)); j++) {
        sonarScanSingleCell(i, j, stackScannedCounter);
      }
    }

    for (int i = x + 1; i <= x + 3; i++) {
      for (int j = y - (3 - (i - x)); j <= y + (3 - (i - x)); j++) {
        sonarScanSingleCell(i, j, stackScannedCounter);
      }
    }

    return new SonarScanResult(stackScannedCounter);
  }

  /**
   * helper function for sonar scan
   *
   * @param x x coordinate
   * @param y y coordinate
   * @param stackScannedCounter map stack to count
   */
  private void sonarScanSingleCell(int x, int y, Map<StackType, Integer> stackScannedCounter) {
    Character chr = getIfWithinBound(x, y);
    if (chr != null && chr != '*' && chr != ' ') {
      StackType curCellStackType = coordinateToStack.get(new Pair<>(x, y)).getStackType();
      stackScannedCounter.compute(curCellStackType, (key, val) -> {
        if (val == null) {
          return 1;
        } else {
          return val + 1;
        }
      });
    }
  }


  public Character getIfWithinBound(int x, int y) {
    if (x >= 0 && x < boardMatrix.length && y >= 0 && y < boardMatrix[0].length) {
      return boardMatrix[x][y];
    }
    return null;
  }

  /**
   * check if we have already find all the stacks
   * @return true if we already found all the stack
   */
  public boolean foundAll() {
    return foundCells == numCells;
  }
  
  public boolean moveAStack(Stack stack, Direction direction, Pair<Integer, Integer> targetCoordinate) {
    Stack newStack = stackFactory.getStack(stack.getStackType(), new Parser.PlaceStackInfo(targetCoordinate.getKey(), targetCoordinate.getValue(), direction));
    newStack.found = stack.found;

    if (newStack.checkIfPositionValid(boardMatrix)) {
      stack.removeFromBoard(boardMatrix, coordinateToStack);
      newStack.putStackOnBoard(boardMatrix, coordinateToStack);
      return true;
    }

    return false;
  }

  
}
