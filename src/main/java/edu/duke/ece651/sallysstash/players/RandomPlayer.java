package edu.duke.ece651.sallysstash.players;

import edu.duke.ece651.sallysstash.controllers.*;
import edu.duke.ece651.sallysstash.input.Parser;
import edu.duke.ece651.sallysstash.input.Validator;
import edu.duke.ece651.sallysstash.stacks.StackType;
import edu.duke.ece651.sallysstash.views.DigASquareView;
import edu.duke.ece651.sallysstash.views.DoubleBoardsView;
import edu.duke.ece651.sallysstash.views.MoveChoiceView;
import edu.duke.ece651.sallysstash.views.SingleBoardView;
import javafx.util.Pair;

import java.util.Map;
import java.util.Random;

public class RandomPlayer extends Player {

  String[] irregularDirection = new String[]{"U", "D", "R", "L"};
  String[] rectangularDirection = new String[]{"H", "V"};
  SingleBoardView singleBoardView = new SingleBoardView();
  MoveChoiceView moveChoiceView = new MoveChoiceView();
  DigASquareView digASquareView = new DigASquareView();
  DoubleBoardsView doubleBoardsView = new DoubleBoardsView();
  Random random;
  Pair<Integer, Integer> curDigSquare;
  boolean isDebug;

  public RandomPlayer(String name, boolean isDebug) {
    super(name);
    remainingNewMoves = 3;
    this.isDebug = isDebug;
    random = new Random();
  }

  private String getRandomPlaceInfo(StackType stackType) {
    String row = String.valueOf((char) (random.nextInt(20) + 'A'));
    String col = String.valueOf((char) (random.nextInt(10) + '0'));
    String direction = "";
    if (stackType == StackType.RED || stackType == StackType.BLUE) {
      direction = irregularDirection[random.nextInt(4)];
    } else {
      direction = rectangularDirection[random.nextInt(2)];
    }
    return row + col + direction;
  }

  private Pair<Integer, Integer> getRandomCoordinate() {
    return new Pair<>(random.nextInt(20), random.nextInt(10));
  }

  /**
   * place the stash for the player
   *
   * @param placeStashController placeStashController
   */
  @Override
  public void placeStashSinglePlayer(PlaceStashController placeStashController) {
    for (Map.Entry<StackType, Integer> entry : stashPlaceMap.entrySet()) {
      for (int i = 0; i < entry.getValue(); i++) {
        placeAStack(entry.getKey(), i, placeStashController);
      }
    }
  }

  /**
   * randomly place a single stack, handle users input. Only when coordinate + direction is valid, the stack will be placed
   *
   * @param stackType the type of the stack to place
   * @param stackIdx the index of the stack to place
   * @param placeStashController placeStashController
   */
  @Override
  public void placeAStack(StackType stackType, int stackIdx, PlaceStashController placeStashController) {
    while (true) {
      String randomInput = getRandomPlaceInfo(stackType);
      if (Validator.isValidPlaceStashInput(stackType, randomInput)) {
        Parser.PlaceStackInfo placeStackInfo = Parser.parsePlaceStashInput(randomInput);
        if (board.placeStash(stackType, placeStackInfo)) {
          break;
        }
      }
    }
    if (isDebug) {
      singleBoardView.display(this.board);
    }
  }

  @Override
  public String getMoveOption(MoveChoiceController moveChoiceController, Player opponent) {
    String str = String.valueOf(random.nextInt(3) + 1);
    if (isDebug) {
      System.out.println("Computer input: " + str);
    }
    return str;
  }

  /**
   * randomly generate a coordinate to dig
   *
   * @param digASquareController digSquareController
   * @param opponent the current player's opponent
   * @return return the coordinate player want to dig
   */
  @Override
  public Pair<Integer, Integer> digASquare(DigASquareController digASquareController, Player opponent) {
    curDigSquare = getRandomCoordinate();
    if (isDebug) {
      System.out.printf("Computer Input: %c%c\n", (char)(curDigSquare.getKey() + 'A'), (char)(curDigSquare.getValue() + '0'));
      doubleBoardsView.display(this.board, opponent.board);
    }
    return curDigSquare;
  }

  @Override
  public Pair<Integer, Integer> getACoordinateOnStack(MoveStackController moveStackController) {
    Pair<Integer, Integer> pair = getRandomCoordinate();
    if (isDebug) {
      System.out.printf("Computer Input: %c%c\n", (char)(pair.getKey() + 'A'), (char)(pair.getValue() + '0'));
    }
    return pair;
  }

  @Override
  public Parser.MoveInfo getMoveInfo(MoveStackController moveStackController) {
    String moveInfoString = getRandomPlaceInfo(moveStackController.hitStack.getStackType());
    if (isDebug) {
      System.out.println("Computer Input: " + moveInfoString);
    }
    return Parser.parseMoveToTargetInput(moveInfoString);
  }

  @Override
  public Pair<Integer, Integer> getSonarScanCoordinate(SonarScanController sonarScanController) {
    Pair<Integer, Integer> pair = getRandomCoordinate();
    if (isDebug) {
      System.out.printf("Computer Input: %c%c\n", (char)(pair.getKey() + 'A'), (char)(pair.getValue() + '0'));
    }
    return pair;
  }

  @Override
  public void whosTurnPrompt() {
    moveChoiceView.whosTurnPromptComputer(this, isDebug);
  }

  /**
   * prompt if the player find a stack
   *
   * @param isHit true if the player find a stack else false
   * @param opponent current player's opponent
   */
  @Override
  public void isHitPrompt(boolean isHit, Player opponent) {
    digASquareView.isHitPromptComputer(isHit, opponent.board.coordinateToStack.get(curDigSquare), curDigSquare);
  }

  @Override
  public void specialMovePrompt() {
    moveChoiceView.specialMovePrompt(this);
  }

  @Override
  public void receiveSonarScanResult(Board.SonarScanResult sonarScanResult) {}
}
