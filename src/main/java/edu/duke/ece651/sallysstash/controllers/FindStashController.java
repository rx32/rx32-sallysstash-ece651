package edu.duke.ece651.sallysstash.controllers;


import edu.duke.ece651.sallysstash.players.Player;
import edu.duke.ece651.sallysstash.views.FindStashView;

import java.util.Scanner;


public class FindStashController {

  MoveChoiceController moveChoiceController;
  MoveStackController moveStackController;
  FindStashView findStashView;

  public FindStashController(Scanner scanner) {
    moveStackController = new MoveStackController(scanner);
    moveChoiceController = new MoveChoiceController(scanner, this);
    findStashView = new FindStashView();
  }

  /**
   * player 1 and player 2 take turns to make move
   * @param player1 player A
   * @param player2 player B
   */
  public void selectMove(Player player1, Player player2) {
    findStashView.findStashPrompt();;
    int turn = 0;
    while (!player1.isWin() && !player2.isWin()) {
      if (turn == 0) {
        moveChoiceController.selectMove(player1, player2);
        player1.setWin(player2.board.foundAll());
      } else {
        moveChoiceController.selectMove(player2, player1);
        player2.setWin(player1.board.foundAll());
      }
      turn = 1 - turn;
    }
  }

}
