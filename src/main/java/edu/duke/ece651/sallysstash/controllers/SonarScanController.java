package edu.duke.ece651.sallysstash.controllers;

import edu.duke.ece651.sallysstash.players.Board;
import edu.duke.ece651.sallysstash.players.Player;
import edu.duke.ece651.sallysstash.input.Parser;
import edu.duke.ece651.sallysstash.input.Validator;
import edu.duke.ece651.sallysstash.views.SonarScanView;
import javafx.util.Pair;

import java.util.Scanner;


public class SonarScanController {

  Scanner scanner;
  SonarScanView sonarScanView;
  SonarScanController(Scanner scanner) {
    this.scanner = scanner;
    sonarScanView = new SonarScanView();
  }

  public void sonarScan(Player player, Player opponent, MoveChoiceController moveChoiceController) {
      Pair<Integer, Integer> coordinate = player.getSonarScanCoordinate(this);
      Board.SonarScanResult sonarScanResult = opponent.board.sonarScan(coordinate);
      player.receiveSonarScanResult(sonarScanResult);
      player.decreaseRemainingNewMoves();
      moveChoiceController.setIsSpecialMove();
  }

  /**
   * get user input if the Board object need to interact with user
   *
   * @return the coordinate the user wants to scan
   */
  public Pair<Integer, Integer> getUserInputSonarScanCoordinate() {
    sonarScanView.display();
    String curInput = scanner.nextLine();
    while(!Validator.isValidSonarScanInput(curInput)) {
      sonarScanView.invalidInputPrompt();
      sonarScanView.display();
      curInput = scanner.nextLine();
    }
    return Parser.parseSonarScanInput(curInput);
  }

}
