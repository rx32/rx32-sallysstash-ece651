package edu.duke.ece651.sallysstash.controllers;

import edu.duke.ece651.sallysstash.players.Player;
import edu.duke.ece651.sallysstash.input.Validator;
import edu.duke.ece651.sallysstash.views.DoubleBoardsView;
import edu.duke.ece651.sallysstash.views.MoveChoiceView;

import java.util.Scanner;

public class MoveChoiceController {

  Scanner scanner;
  public MoveChoiceView moveChoiceView;
  public FindStashController findStashController;
  public DigASquareController digASquareController;
  public MoveStackController moveStackController;
  public SonarScanController sonarScanController;
  public DoubleBoardsView doubleBoardsView;
  public boolean continueSelectingMove;
  public boolean moveStackFailed;
  private boolean isSpecialMove;

  public MoveChoiceController(Scanner scanner, FindStashController findStashController) {

    this.scanner = scanner;
    this.continueSelectingMove = true;
    this.findStashController = findStashController;
    this.digASquareController = new DigASquareController(scanner);
    this.moveStackController = new MoveStackController(scanner);
    this.sonarScanController = new SonarScanController(scanner);
    moveChoiceView = new MoveChoiceView();
    doubleBoardsView = new DoubleBoardsView();

  }

  /**
   * get a valid option input from user
   *
   * @return input string after validation
   */
  public String getValidOptionInput() {
    String option = scanner.nextLine();
    while (!Validator.isValidMoveChoice(option)) {
      moveChoiceView.invalidInputPrompt();
      option = scanner.nextLine();
    }
    return option;
  }

  /**
   * according to user's move option, make the corresponding move
   *
   * @param player current player
   * @param opponent current player's opponent
   * @param option user's option
   */
  void executeMove(Player player, Player opponent, String option) {
    switch (option) {
      case "1":
        digASquareController.digASquare(player, opponent);
        continueSelectingMove = false;
        break;
      case "2":
        moveStackController.moveStack(player, opponent, this);
        break;
      case "3":
        sonarScanController.sonarScan(player, opponent, this);
        break;
      default:
        break;
    }
  }

  /**
   * when a user choose move stack and selected a stash to move successfully but he chooses a target coordinate and direction that is conflict with the stacks on the board
   *
   * @param player current player
   * @param opponent current player's opponent
   * @param option current player's option
   */
  void executeMoveAfterMoveStackFailed(Player player, Player opponent, String option) {
    switch (option) {
      case "1":
        digASquareController.digASquare(player, opponent);
        continueSelectingMove = false;
        break;
      case "2":
        moveStackController.moveToTargetLocation(player, this);
        break;
      case "3":
        sonarScanController.sonarScan(player, opponent, this);
        break;
      default:
        break;
    }
  }

  /**
   * set moveStackFailed flag
   */
  void selectMoveAfterMoveStackFailed() {
    moveStackFailed = true;
  }

  /**
   * set moveStackFailed flag
   */
  void selectMoveAfterMoveStackSucceed() {
    moveStackFailed = false;
  }

  /**
   * set isSpecialMove flag
   */
  void setIsSpecialMove() {
    isSpecialMove = true;
  }

  /**
   * select move based on three state flags: continueSelectingMove, isSpecialMove and moveStackFailed
   *
   * @param player the current player
   * @param opponent current player's opponent
   */
  void selectMove(Player player, Player opponent) {
    continueSelectingMove = true;
    moveStackFailed = false;
    isSpecialMove = false;
    while (continueSelectingMove) {
      player.whosTurnPrompt();
      if (player.getRemainingNewMoves() > 0) {
        String option = player.getMoveOption(this, opponent);
        if (!moveStackFailed) {
          executeMove(player, opponent, option);
        } else {
          executeMoveAfterMoveStackFailed(player, opponent, option);
        }
        if (isSpecialMove) {
          player.specialMovePrompt();
        }
      } else {
        digASquareController.digASquare(player, opponent);
        continueSelectingMove = false;
      }
    }
  }

}
