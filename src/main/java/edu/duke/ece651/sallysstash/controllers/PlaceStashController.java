package edu.duke.ece651.sallysstash.controllers;

import edu.duke.ece651.sallysstash.players.Player;
import edu.duke.ece651.sallysstash.input.Parser;
import edu.duke.ece651.sallysstash.input.Validator;
import edu.duke.ece651.sallysstash.stacks.StackType;
import edu.duke.ece651.sallysstash.views.PlaceStashView;

import java.util.Scanner;


public class PlaceStashController {

  PlaceStashView placeStashView;
  Scanner scanner;

  public PlaceStashController(Scanner scanner) {
    this.scanner = scanner;
    placeStashView = new PlaceStashView();
  }

  /**
   * control players to place stashes(call api exposed by the player to change the player's states)
   *
   * @param player1 player A
   * @param player2 player B
   */
  public void placeAllStash(Player player1, Player player2) {
    player1.placeStashSinglePlayer(this);
    player2.placeStashSinglePlayer(this);
  }

  /**
   * receive the user input if the Player object needs to interact with user
   *
   * @param stackType current stack's type
   * @param player current player
   * @return return if the player successfully put the stack
   */
  public boolean handleInput(StackType stackType, Player player) {
    String curInput = scanner.nextLine();
    if (Validator.isValidPlaceStashInput(stackType, curInput)) {
      Parser.PlaceStackInfo placeStackInfo = Parser.parsePlaceStashInput(curInput);
      return player.board.placeStash(stackType, placeStackInfo);
    } else {
      return false;
    }
  }

}
