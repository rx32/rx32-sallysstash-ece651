package edu.duke.ece651.sallysstash.controllers;

import edu.duke.ece651.sallysstash.players.Player;
import edu.duke.ece651.sallysstash.input.Parser;
import edu.duke.ece651.sallysstash.input.Validator;
import edu.duke.ece651.sallysstash.views.DigASquareView;
import javafx.util.Pair;

import java.util.Scanner;

public class DigASquareController {

  Scanner scanner;
  DigASquareView digASquareView;

  DigASquareController(Scanner scanner) {
    this.scanner = scanner;
    digASquareView = new DigASquareView();
  }

  /**
   * call player's api to get the coordinate of square to dig (change player's state)
   * call the api of player's opponent to check if the player find a square or not
   *
   * @param player the current player
   * @param opponent the player's opponent
   */
  public void digASquare(Player player, Player opponent) {
    Pair<Integer, Integer> coordinate = player.digASquare(this, opponent);
    boolean isHit = opponent.board.findStash(coordinate);
    player.isHitPrompt(isHit, opponent);
  }

  /**
   * help user to receive input if the Player object need to interact with user
   *
   * @return the coordinate the user input
   */
  public Pair<Integer, Integer> getInputCoordinate() {
    String curInput = scanner.nextLine();
    digASquareView.showInput(curInput);
    while (!Validator.isValidFindStashInput(curInput)) {
      digASquareView.invalidInputPrompt();
      curInput = scanner.nextLine();
      digASquareView.showInput(curInput);
    }
    return Parser.parseFindStashInput(curInput);
  }



}
