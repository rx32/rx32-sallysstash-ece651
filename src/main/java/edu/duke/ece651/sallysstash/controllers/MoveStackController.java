package edu.duke.ece651.sallysstash.controllers;

import edu.duke.ece651.sallysstash.players.Player;
import edu.duke.ece651.sallysstash.input.Parser;
import edu.duke.ece651.sallysstash.input.Validator;
import edu.duke.ece651.sallysstash.stacks.Direction;
import edu.duke.ece651.sallysstash.stacks.Stack;
import edu.duke.ece651.sallysstash.views.MoveStackView;
import javafx.util.Pair;

import java.util.Scanner;

public class MoveStackController {

  Scanner scanner;
  MoveStackView moveStackView;
  public Stack hitStack;

  MoveStackController(Scanner scanner) {
    this.scanner = scanner;
    this.moveStackView = new MoveStackView();
  }

  /**
   * get user input coordinate, expose api for Player object to interact with uesr
   * @return coordinate
   */
  public Pair<Integer, Integer> getInputCoordinate() {
    moveStackView.displayInputCoordinatePrompt();
    String input = scanner.nextLine();
    while (!Validator.isValidChooseStackInput(input)) {
      input = scanner.nextLine();
      moveStackView.displayInputCoordinatePrompt();
    }
    return Parser.parseChooseStackInput(input);
  }

  /**
   * coordinate and direction specified by user
   *
   * @return (coordinate + direction) of stack
   */
  public Parser.MoveInfo getInputMoveInfo() {
    moveStackView.displayInputMoveInfoPrompt(hitStack);
    String input = scanner.nextLine();
    while (!Validator.isValidParseMoveToTargetInput(hitStack.getStackType(), input)) {
      moveStackView.displayInputMoveInfoPrompt(hitStack);
      input = scanner.nextLine();
    }
    return Parser.parseMoveToTargetInput(input);
  }

  /**
   * control player to select a stack to move
   *
   * @param player current user
   * @param opponent current user's opponent
   * @param moveChoiceController moveChoiceController
   */
  public void moveStack(Player player, Player opponent, MoveChoiceController moveChoiceController) {
      Pair<Integer, Integer> coordinate = player.getACoordinateOnStack(this);
      hitStack = player.board.getHitStack(coordinate);
      while (hitStack == null) {
        coordinate = player.getACoordinateOnStack(this);
        hitStack = player.board.getHitStack(coordinate);
      }
      moveToTargetLocation(player, moveChoiceController);
  }

  /**
   * move the selected stack to target location
   *
   * @param player current player
   * @param moveChoiceController moveChoiceController object
   */
  public void moveToTargetLocation(Player player, MoveChoiceController moveChoiceController) {
    Parser.MoveInfo moveInfo = player.getMoveInfo(this);
    Direction direction = moveInfo.direction;
    Pair<Integer, Integer> targetCoordinate = moveInfo.targetCoordinate;
    if (!player.board.moveAStack(hitStack, direction, targetCoordinate)) {
      moveChoiceController.selectMoveAfterMoveStackFailed();
    } else {
      moveChoiceController.selectMoveAfterMoveStackSucceed();
      moveChoiceController.setIsSpecialMove();
      player.decreaseRemainingNewMoves();
      hitStack = null;
    }
  }

}
