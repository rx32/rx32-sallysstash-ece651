package edu.duke.ece651.sallysstash.views;

import edu.duke.ece651.sallysstash.Game;
import edu.duke.ece651.sallysstash.MenuOptions;

import java.util.Scanner;

import static edu.duke.ece651.sallysstash.MenuOptions.*;

public class MenuView {
  Scanner scanner;
  Game game;
  public MenuView(Scanner scanner, Game game) {
    this.game = game;
    this.scanner = scanner;
  }

  /**
   * display the main menu
   */
  public void display() {
    System.out.println("Enter 1: new game(player vs player);");
    System.out.println("Enter 2: new game(player vs computer);");
    System.out.println("Enter 3: new game(computer vs player);");
    System.out.println("Enter 4: new game(computer vs computer);");
    System.out.println("Enter 5: exit;");
    System.out.println("Enter 6: toggle verbose output;");

  }

  /**
   * prompt when user enters invalid input
   */
  void handleInvalidInput() {
    System.out.println("Invalid input, please enter your choice again.");
  }

  /**
   * parse the user input to menu option
   *
   * @return menu option user chooses
   */
  public MenuOptions handleInput() {
    String options = scanner.nextLine();
    switch (options) {
      case "1":
        game.newGameHumanVSHuman();
        return NEW_GAME_HUMAN_VS_HUMAN;
      case "2":
        game.newGameHumanVSComputer();
        return NEW_GAME_HUMAN_VS_COUMPUTER;
      case "3":
        game.newGameComputerVSHuman();
        return NEW_GAME_COMPUTER_VS_HUMAN;
      case "4":
        game.newGameComputerVSComputer();
        return NEW_GAME_COMPUTER_VS_COUMPUTER;
      case "5":
        return EXIT;
      case "6":
        game.isDebug = !game.isDebug;
      default:
        handleInvalidInput();
        return INVALID;
    }
  }


}
