package edu.duke.ece651.sallysstash.views;

public class FindStashView {

  /**
   * prompt to separate the placing stage and the find stage
   */
  public void findStashPrompt() {
    System.out.println("\n\n-----------------------------------------------------------------------");
    System.out.println("                           Start to find stash                             ");
    System.out.println("-----------------------------------------------------------------------\n\n");

  }
}
