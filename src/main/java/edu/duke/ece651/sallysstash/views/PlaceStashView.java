package edu.duke.ece651.sallysstash.views;

import edu.duke.ece651.sallysstash.players.Player;
import edu.duke.ece651.sallysstash.stacks.StackType;

import java.util.HashMap;
import java.util.Map;

import static edu.duke.ece651.sallysstash.stacks.StackType.*;

public class PlaceStashView {

  Map<StackType, String> nameMap = new HashMap<>() {{
    put(GREEN, "Green");
    put(PURPLE, "Purple");
    put(RED, "RED");
    put(BLUE, "Blue");
  }};

  Map<Integer, String> numMap = new HashMap<>(){{
    put(0, "First");
    put(1, "Second");
    put(2, "Third");
    put(3, "Fourth");
    put(4, "Fifth");
    put(5, "Sixth");
  }};

  SingleBoardView singleBoardView;

  public PlaceStashView() {
    singleBoardView = new SingleBoardView();
  }

  /**
   * prompt for user placing stacks
   *
   * @param stackType stack type
   * @param stackIdx  which stack the user is putting. first, second, third...
   * @param player the current user
   */
  public void display(StackType stackType, int stackIdx, Player player) {
    System.out.println("----------------------------Place Stash----------------------------");
    System.out.println(String.format("Player %S, where do you want to place the %s %s stack", player.getName(), nameMap.get(stackType), numMap.get(stackIdx)));
    displayBoard(player);
    System.out.println("-------------------------------------------------------------------\n\n");
  }

  /**
   * display player's board
   *
   * @param player current player
   */
  public void displayBoard(Player player) {
    singleBoardView.display(player.board);
  }

  /**
   * prompt if user enters invalid input
   */
  public void invalidInputPrompt() {
    System.out.println("Invalid input, please enter the stack position and direction again.");
  }

  /**
   * display final board
   */
  public void finalBoardPrompt() {
    System.out.println("Your final Board is:");
  }

}
