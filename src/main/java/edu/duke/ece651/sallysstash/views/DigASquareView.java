package edu.duke.ece651.sallysstash.views;

import edu.duke.ece651.sallysstash.players.Player;
import edu.duke.ece651.sallysstash.stacks.Stack;
import javafx.util.Pair;

public class DigASquareView {

  DoubleBoardsView doubleBoardsView;
  String curInput;

  public DigASquareView() {
    doubleBoardsView = new DoubleBoardsView();
  }

  /**
   * display players' board and prompts
   *
   * @param player the current player
   * @param opponent the current player's opponent
   */
  public void display(Player player, Player opponent) {
    System.out.println("--------------------------------Find Stash---------------------------------");
    System.out.println(String.format("Player %S's turn: ", player.getName()));
    displayBoard(player, opponent);
    System.out.println("---------------------------------------------------------------------------\n\n");

  }

  /**
   * display player's own board and the opponent's board
   *
   * @param player the current player
   * @param opponent the current player's opponent
   */
  void displayBoard(Player player, Player opponent) {
    System.out.println(String.format("     Your tree                                            Player %s' tree", opponent.getName()));
    doubleBoardsView.display(player.board, opponent.board);
  }

  /**
   * if the user enters invalid input, show the prompt
   */
  public void invalidInputPrompt() {
    System.out.println(String.format("Your input: %s is invalid. Please enter valid coordinate!", curInput));
  }

  /**
   * display user's input
   *
   * @param input user's input
   */
  public void showInput(String input) {
    System.out.println("Your input: " + input);
  }

  /**
   * if human player find a stack, show the player corresponding prompt
   *
   * @param isHit if the player hit one of the square in the stash
   */
  public void isHitPromptHuman(boolean isHit) {
    if (isHit) {
      System.out.println("You found a stack!\n\n");
    } else {
      System.out.println("You missed!\n\n");
    }
  }

  /**
   * prompt after computer take its move.
   *
   * @param isHit flag: if the player hit one of the square in the stash
   * @param stack the hit stack, if isHit is false, then stack is null
   * @param coordinate the coordinate player specifies
   */
  public void isHitPromptComputer(boolean isHit, Stack stack, Pair<Integer, Integer> coordinate) {
    if (isHit) {
      String coordinateStr = (char) (coordinate.getKey() + 'A') + String.valueOf((char)(coordinate.getValue() + '0'));
      System.out.printf("Computer found a %s stack at %s!\n\n", stack.getName(), coordinateStr);
    } else {
      System.out.println("Computer missed!\n\n");
    }
  }


}
