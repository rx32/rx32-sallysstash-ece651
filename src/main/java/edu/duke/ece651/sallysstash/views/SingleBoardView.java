package edu.duke.ece651.sallysstash.views;

import edu.duke.ece651.sallysstash.players.Board;

public class SingleBoardView {

  public SingleBoardView(){}

  /**
   * display the board
   *
   * @param board the baord to display
   */
  public void display(Board board) {
    char[][] boardMatrix = board.getBoardMatrix();

    displayFirstAndLastRow(boardMatrix);
    for (int i = 0; i < boardMatrix.length; i++) {
      System.out.print((char) (i + 'A'));
      for (int j = 0; j < boardMatrix[0].length; j++) {
        System.out.print(boardMatrix[i][j] + (j == boardMatrix[0].length - 1 ? "" : "|"));
      }
      System.out.println((char) (i + 'A'));
    }
    displayFirstAndLastRow(boardMatrix);
  }

  /**
   * display the first row and last row
   *
   * @param boardMatrix the board to display
   */
  private void displayFirstAndLastRow(char[][] boardMatrix) {
    System.out.print(" ");
    for (int j = 0; j < boardMatrix[0].length; j++) {
      System.out.print(j + (j == boardMatrix[0].length - 1 ? "" : "|"));
    }
    System.out.println();
  }

}
