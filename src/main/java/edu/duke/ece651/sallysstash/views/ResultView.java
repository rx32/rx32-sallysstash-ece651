package edu.duke.ece651.sallysstash.views;

import edu.duke.ece651.sallysstash.players.Player;

public class ResultView {

  /**
   * display who wins the game
   *
   * @param player winner
   */
  public void getView(Player player) {
    System.out.println(player.getName() + " win!");
  }
}
