package edu.duke.ece651.sallysstash.views;

import edu.duke.ece651.sallysstash.stacks.Stack;

public class MoveStackView {


  public void noNewMovesPrompt() {
    System.out.println("No new moves.. You have already used new moves three times.");
  }

  public void displayInputCoordinatePrompt() {
    System.out.println("Please Select Stack: \n");
  }

  public void displayInputMoveInfoPrompt(Stack stack) {
    System.out.println(String.format("You select a %s stack.", stack.getName()));
    System.out.println("Please Enter where you want to place this Stack: \n");
  }

}
