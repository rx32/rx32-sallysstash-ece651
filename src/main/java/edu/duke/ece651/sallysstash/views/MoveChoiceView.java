package edu.duke.ece651.sallysstash.views;

import edu.duke.ece651.sallysstash.players.HumanPlayer;
import edu.duke.ece651.sallysstash.players.Player;
import edu.duke.ece651.sallysstash.controllers.MoveChoiceController;


public class MoveChoiceView {

  public MoveChoiceView() {}

  public void displayMoveChoice(Player player, MoveChoiceController moveChoiceController) {
    if (moveChoiceController.moveStackFailed) {
      System.out.println("Enter 1: Dig a cell;");
      System.out.println("Enter 2: Enter a valid location;");
      System.out.println("Enter 3: Sonar scan;");
    } else {
      System.out.println("Enter 1: Dig a cell;");
      System.out.println("Enter 2: Move stack;");
      System.out.println("Enter 3: Sonar scan;");
    }
  }

  public void invalidInputPrompt() {
    System.out.println("Invalid input, please enter your choice again.");
  }

  public void whosTurnPromptHuman(Player player) {
    System.out.println(String.format("Player %S's turn: ", player.getName()));
    if (player.getRemainingNewMoves() > 0) {
      System.out.printf("You have %d new moves.\n", player.getRemainingNewMoves());
    } else {
      System.out.println("You do not have new moves. Please choose the square you want to dig.");
    }
  }

  public void whosTurnPromptComputer(Player player, boolean isDebug) {
    if (isDebug) {
      System.out.printf("Computer has %d new moves.\n", player.getRemainingNewMoves());
    }
    System.out.println(String.format("Player %S's turn: ", player.getName()));
  }

  public void specialMovePrompt(Player player) {
    System.out.printf("%s used one special move.\n", player.getName());
  }


}

