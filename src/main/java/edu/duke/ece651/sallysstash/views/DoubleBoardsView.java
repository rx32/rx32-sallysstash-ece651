package edu.duke.ece651.sallysstash.views;

import edu.duke.ece651.sallysstash.players.Board;

public class DoubleBoardsView {

  String whiteSpaceTitle = "                                  ";
  String whiteSpaceBoard = "                                 ";

  public DoubleBoardsView() {}

  /**
   * display players' board
   *
   * @param myBoard current player's board
   * @param opponentBoard opponent's board
   */
  public void display(Board myBoard, Board opponentBoard) {
    char[][] myBoardMatrix = myBoard.getBoardMatrix();
    char[][] opponentBoardMatrix = opponentBoard.getBoardMatrixForOpponent();

    displayFirstAndLastRow(myBoardMatrix);
    for (int i = 0; i < myBoardMatrix.length; i++) {
      System.out.print((char) (i + 'A'));
      for (int j = 0; j < myBoardMatrix[0].length; j++) {
        System.out.print(myBoardMatrix[i][j] + (j == myBoardMatrix[0].length - 1 ? "" : "|"));
      }
      System.out.print((char) (i + 'A'));
      System.out.print(whiteSpaceBoard);
      System.out.print((char) (i + 'A'));
      for (int j = 0; j < opponentBoardMatrix[0].length; j++) {
        System.out.print(opponentBoardMatrix[i][j] + (j == opponentBoardMatrix[0].length - 1 ? "" : "|"));
      }
      System.out.println((char) (i + 'A'));
    }
    displayFirstAndLastRow(myBoardMatrix);

  }

  /**
   * display the first row and the last row of the current board
   *
   * @param boardMatrix board matrix to display
   */
  private void displayFirstAndLastRow(char[][] boardMatrix) {
    displayHalfTitle(boardMatrix);
    System.out.print(whiteSpaceTitle);
    displayHalfTitle(boardMatrix);
    System.out.println();
  }

  /**
   * display the first row and the last row of a board
   *
   * @param boardMatrix board matrix to display
   */
  private void displayHalfTitle(char[][] boardMatrix) {
    System.out.print(" ");
    for (int j = 0; j < boardMatrix[0].length; j++) {
      System.out.print(j + (j == boardMatrix[0].length - 1 ? "" : "|"));
    }
  }


}
