package edu.duke.ece651.sallysstash.views;

import edu.duke.ece651.sallysstash.players.Board;

public class SonarScanView {
  String curInput;
  public SonarScanView() {}

  public void display() {
    System.out.println("Please Enter the center point of the area you want to scan.");
  }

  public void invalidInputPrompt() {
    System.out.println("Please Enter valid input.");
  }

  public void displaySonarScanResult(Board.SonarScanResult sonarScanResult) {
    System.out.println(String.format("Green stacks occupy %d squares.", sonarScanResult.numGreen));
    System.out.println(String.format("Purple stacks occupy %d squares.", sonarScanResult.numPurple));
    System.out.println(String.format("Red stacks occupy %d squares.", sonarScanResult.numRed));
    System.out.println(String.format("Blue stacks occupy %d squares.", sonarScanResult.numBlue));
  }

  public void noNewMovesPrompt() {
    System.out.println("No new moves.. You have already used new moves three times.");
  }

}
