package edu.duke.ece651.sallysstash.views;

public interface View {
  void getView();
}
