package edu.duke.ece651.sallysstash;

import edu.duke.ece651.sallysstash.controllers.FindStashController;
import edu.duke.ece651.sallysstash.controllers.PlaceStashController;
import edu.duke.ece651.sallysstash.players.HumanPlayer;
import edu.duke.ece651.sallysstash.players.Player;
import edu.duke.ece651.sallysstash.players.RandomPlayer;
import edu.duke.ece651.sallysstash.views.MenuView;
import edu.duke.ece651.sallysstash.views.ResultView;

import java.util.Scanner;

import static edu.duke.ece651.sallysstash.MenuOptions.EXIT;
import static edu.duke.ece651.sallysstash.MenuOptions.INVALID;

public class Game {
  Player player1;
  Player player2;
  MenuView menuView;
  ResultView resultView;
  FindStashController findStashController;
  PlaceStashController placeStashController;
  Scanner placeInputScanner;
  Scanner findInputScanner;
  public boolean isDebug;


  Game(Scanner placeInputScanner, Scanner findInputScanner, boolean isDebug) {
    this.placeInputScanner = placeInputScanner;
    this.findInputScanner = findInputScanner;
    menuView = new MenuView(placeInputScanner, this);
    resultView = new ResultView();
    this.isDebug = isDebug;
  }

  /**
   * game entrance. show the menu to user.
   */
  public void run() {
    while (true) {
      menuView.display();
      MenuOptions op = menuView.handleInput();
      if (op == EXIT) {
        return;
      } else if (op == INVALID) {
        continue;
      }
      placeAllStash();
      findAllStash();
      gameResult();
    }

  }

  /**
   * create a new game for two human players
   */
  public void newGameHumanVSHuman() {
    player1 = new HumanPlayer("A");
    player2 = new HumanPlayer("B");
    placeStashController = new PlaceStashController(placeInputScanner);
    findStashController = new FindStashController(findInputScanner);
  }

  public void newGameHumanVSComputer() {
    player1 = new HumanPlayer("A");
    player2 = new RandomPlayer("B", isDebug);
    placeStashController = new PlaceStashController(placeInputScanner);
    findStashController = new FindStashController(findInputScanner);
  }

  public void newGameComputerVSHuman() {
    player1 = new RandomPlayer("A", isDebug);
    player2 = new HumanPlayer("B");
    placeStashController = new PlaceStashController(placeInputScanner);
    findStashController = new FindStashController(findInputScanner);
  }

  /**
   * create a new game for testing. Both players are computers taking random move.
   */
  public void newGameComputerVSComputer() {
    player1 = new RandomPlayer("A", isDebug);
    player2 = new RandomPlayer("B", isDebug);
    placeStashController = new PlaceStashController(placeInputScanner);
    findStashController = new FindStashController(findInputScanner);
  }

  /**
   * game stage 1: place stashes
   */
  public void placeAllStash() {
    placeStashController.placeAllStash(player1, player2);
  }

  /**
   * game stage 2: players take turns to find stashes
   */
  public void findAllStash() {
    findStashController.selectMove(player1, player2);
  }

  /**
   * show game result
   */
  public void gameResult() {
    if (player1.isWin()) {
      resultView.getView(player1);
    } else if (player2.isWin()) {
      resultView.getView(player2);
    }
  }
}
