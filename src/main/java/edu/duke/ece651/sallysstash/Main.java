package edu.duke.ece651.sallysstash;

import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    boolean isDebug = false;
    Game game = new Game(new Scanner(System.in), new Scanner(System.in), isDebug);
    game.run();
  }
}
