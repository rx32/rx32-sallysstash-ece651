package edu.duke.ece651.sallysstash.input;

import edu.duke.ece651.sallysstash.stacks.Direction;
import javafx.util.Pair;

import java.util.HashMap;
import java.util.Map;

public class Parser {

  static Map<Character, Direction> charToDirection = new HashMap<>(){{
    put('V', Direction.VERTICAL);
    put('H', Direction.HORIZONTAL);
    put('U', Direction.UP);
    put('D', Direction.DOWN);
    put('L', Direction.LEFT);
    put('R', Direction.RIGHT);
  }};

  public static class PlaceStackInfo {
    public int x;
    public int y;
    public Direction direction;
    public PlaceStackInfo(int x, int y, Direction direction) {
      this.x = x;
      this.y = y;
      this.direction = direction;
    }
  }

  public static class MoveInfo {
    public Direction direction;
    public Pair<Integer, Integer> targetCoordinate;
    MoveInfo(Direction direction, Pair<Integer, Integer> coordinate) {
      this.direction = direction;
      this.targetCoordinate = coordinate;
    }
  }

  /**
   * parse the user input to x coordinate and y coordinate + direction
   *
   * @param input user input string after validation
   * @return coordinate + direction
   */
  public static PlaceStackInfo parsePlaceStashInput(String input) {
    return new PlaceStackInfo(input.charAt(0) - 'A', input.charAt(1) - '0', charToDirection.get(input.charAt(2)));
  }

  /**
   * parse the user input to coordinate
   *
   * @param input user input string after validation
   * @return coordinate
   */
  public static Pair<Integer, Integer> parseFindStashInput(String input) {
    return parseCoordinate(input);
  }

  /**
   * get the coordinate the user specifies
   *
   * @param input user input after validation
   * @return coordinate
   */
  public static Pair<Integer, Integer> parseSonarScanInput(String input) {
    return parseCoordinate(input);
  }

  /**
   * parse the user input to coordinate
   *
   * @param input user input string after validation
   * @return coordinate
   */
  private static Pair<Integer, Integer> parseCoordinate(String input) {
    return new Pair<>(input.charAt(0) - 'A', input.charAt(1) - '0');
  }

  /**
   * get the coordinate user specifies
   *
   * @param input user input string after validation
   * @return coordinate
   */
  public static Pair<Integer, Integer> parseChooseStackInput(String input) {
    return parseCoordinate(input);
  }

  /**
   *  get the target coordinate and direction
   *
   * @param input user input string after validation
   * @return (coordinate + direction)
   */
  public static MoveInfo parseMoveToTargetInput(String input) {
    return new MoveInfo(charToDirection.get(input.charAt(2)), new Pair<>(input.charAt(0) - 'A', input.charAt(1) - '0'));
  }

}
