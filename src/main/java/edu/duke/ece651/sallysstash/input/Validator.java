package edu.duke.ece651.sallysstash.input;

import edu.duke.ece651.sallysstash.stacks.StackType;

public class Validator {

  /**
   * verify if user input is a valid coordinate
   *
   * @param input user input string
   * @return valid or not
   */
  private static boolean checkCoordinateInput(String input) {
    int x = input.charAt(0) - 'A';
    if (x >= 20 || x < 0) {
      return false;
    }
    int y = input.charAt(1) - '0';
    return y <= 9 && y >= 0;
  }

  /**
   * verify if user input is a valid coordinate + a valid direction
   *
   * @param stackType stack being placed
   * @param input user input string
   * @return valid or not
   */
  public static boolean isValidPlaceStashInput(StackType stackType, String input) {
    if (input.length() != 3) {
      return false;
    }
    if (!checkCoordinateInput(input)) {
      return false;
    }
    char direction = input.charAt(2);
    if (stackType == StackType.GREEN || stackType == StackType.PURPLE) {
      return direction == 'V' || direction == 'H';
    } else {
      return direction == 'U' || direction == 'D' || direction == 'L' || direction == 'R';
    }
  }

  /**
   * verify if user input is a valid coordinate on the board
   *
   * @param input user input string
   * @return valid or not
   */
  public static boolean isValidFindStashInput(String input) {
    return isValidCoordinate(input);
  }

  /**
   * verify if user input is a valid coordinate to scan
   *
   * @param input user input string
   * @return valid or not
   */
  public static boolean isValidSonarScanInput(String input) {
    return isValidCoordinate(input);
  }

  /**
   * helper function to verify if user input is valid
   *
   * @param input user input string
   * @return valid or not
   */
  private static boolean isValidCoordinate(String input) {
    if (input.length() != 2) {
      return false;
    }
    return checkCoordinateInput(input);
  }

  /**
   * validate if the user choose a valid option
   *
   * @param options move menu options
   * @return true if valid
   */
  public static boolean isValidMoveChoice(String options) {
    return options.equals("1") || options.equals("2") || options.equals("3");
  }

  /**
   * check if the user enters a valid coordinate on the board
   *
   * @param input user input string
   * @return true if valid
   */
  public static boolean isValidChooseStackInput(String input) {
    return isValidCoordinate(input);
  }

  /**
   * check if the user enters a valid coordinate + direction
   *
   * @param stackType selected stack's type
   * @param input user input string
   * @return true if valid
   */
  public static boolean isValidParseMoveToTargetInput(StackType stackType, String input) {
    return isValidPlaceStashInput(stackType, input);
  }

}
