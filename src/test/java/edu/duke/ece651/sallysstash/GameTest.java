package edu.duke.ece651.sallysstash;

import org.junit.jupiter.api.Test;

import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {
  @Test
  public void testGame() {
    StringBuilder sbPlaceInput = new StringBuilder();

    sbPlaceInput.append("1\n");

    sbPlaceInput.append("D8V\n");
    sbPlaceInput.append("J1V\n");

    sbPlaceInput.append("B1H\n");
    sbPlaceInput.append("O5H\n");
    sbPlaceInput.append("R9V\n");

    sbPlaceInput.append("D5U\n");
    sbPlaceInput.append("K2R\n");
    sbPlaceInput.append("Q5L\n");

    sbPlaceInput.append("G4U\n");
    sbPlaceInput.append("S0L\n");

    sbPlaceInput.append("D8V\n");
    sbPlaceInput.append("J1V\n");

    sbPlaceInput.append("B1H\n");
    sbPlaceInput.append("O5H\n");
    sbPlaceInput.append("R9V\n");

    sbPlaceInput.append("D5U\n");
    sbPlaceInput.append("K2R\n");
    sbPlaceInput.append("Q5L\n");

    sbPlaceInput.append("G4U\n");
    sbPlaceInput.append("S0L\n");
    sbPlaceInput.append("5\n");

    StringBuilder sbFindInput = new StringBuilder();

    generateInput: for (char i = 'A'; i <= 'T'; i++ ) {
      for (char j = '0'; j <= '9'; j++) {
        sbFindInput.append("1\n").append(String.format("%c%c\n", i, j)).append("1\n").append(String.format("%c%c\n", 'T' - i + 'A', '9' - j + '0'));
        if ('T' - i + 'A' == 'B' && '9' - j + '0' == '1') {
          break generateInput;
        }
      }
    }


    Game game = new Game(new Scanner(sbPlaceInput.toString()), new Scanner(sbFindInput.toString()), true);
    game.run();
  }

  @Test
  public void specialMove() {

    StringBuilder sbPlaceInput = new StringBuilder();
    sbPlaceInput.append("1\n");

    sbPlaceInput.append("D8V\n");
    sbPlaceInput.append("J1V\n");

    sbPlaceInput.append("B1H\n");
    sbPlaceInput.append("O5H\n");
    sbPlaceInput.append("R9V\n");

    sbPlaceInput.append("D5U\n");
    sbPlaceInput.append("K2R\n");
    sbPlaceInput.append("Q5L\n");

    sbPlaceInput.append("G4U\n");
    sbPlaceInput.append("S0L\n");

    sbPlaceInput.append("D8V\n");
    sbPlaceInput.append("J1V\n");

    sbPlaceInput.append("B1H\n");
    sbPlaceInput.append("O5H\n");
    sbPlaceInput.append("R9V\n");

    sbPlaceInput.append("D5U\n");
    sbPlaceInput.append("K2R\n");
    sbPlaceInput.append("Q5L\n");

    sbPlaceInput.append("G4U\n");
    sbPlaceInput.append("S0L\n");
    sbPlaceInput.append("5\n");


    StringBuilder sbFindInput = new StringBuilder();
    sbFindInput.append("2\n").append("B1\n").append("A0V\n");
    sbFindInput.append("2\n").append("E5\n").append("E1L\n");
    sbFindInput.append("3\n").append("L2\n");

    generateInput: for (char i = 'A'; i <= 'T'; i++ ) {
      for (char j = '0'; j <= '9'; j++) {
        sbFindInput.append(String.format("%c%c\n", i, j));
        if (i == 'T' && j == '9') {
          break generateInput;
        }
        sbFindInput.append("1\n").append(String.format("%c%c\n", 'T' - i + 'A', '9' - j + '0'));
      }
    }

    Game game = new Game(new Scanner(sbPlaceInput.toString()), new Scanner(sbFindInput.toString()), true);
    game.run();

  }

  @Test
  public void randomTest() {
    Game game = new Game(new Scanner("4\n4\n4\n4\n4\n5\n"), new Scanner(System.in), true);
    game.run();
  }
}