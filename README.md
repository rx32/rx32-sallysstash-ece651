# rx32-SallysStash-ECE651

1. version 1 commit ID: ee09c41
2. version 2 is the current commit

#### Version 2 

1. choose one option from the game menu

2. place the stack according to the game prompts

3. choose one option from the move menu. 
    a. dig a square
    b. move a stack
    c. use sonar scan

##### Note: 
1.  You only have 3 special moves. Once you used up the special move, you cannot see the move menu.

2. After you choose to move a stack and successfully select a stack you want to move, if you enter a invalid target location + direction of the stack, you can 
    a. choose enter a valid target location + direction
    b. dig a square
    c. use sonar scan. 
    

